const SOCIALS = {
  dilemma: "https://invidious.waifu.bond/watch?v=XisJD8V1Rqw",
  twitter: "https://twitter.com/pp_esports",
  instagram: "https://instagram.com/pp_esports",
  youtube: "https://youtube.com/@pp_esports",
  mastodon: "https://fgc.network/@pp_esports",
  twitch: "https://twitch.tv/pp_esports",
};

export {
  SOCIALS
}